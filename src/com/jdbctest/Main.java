package com.jdbctest;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Main {
	private static ExecutorService executor = null;
    private static volatile Future taskOneResults = null;
    
    public static void blockingThreadPoolExample() {
    	final int poolSize=10,maxPoolSize=20;
    	int threadCnt = 0;
    	final long waitTime = 5000;
    	BlockingQueue<Runnable> bqueue = new LinkedBlockingQueue<Runnable>(50);
    	BlockingThreadPoolExecutor ctpe = new BlockingThreadPoolExecutor(poolSize, maxPoolSize, waitTime, TimeUnit.MILLISECONDS, bqueue);
    	
    	ctpe.setRejectedExecutionHandler((r,x)->{
    		System.out.println("MyThread Rejected : " + ((MyThread) r).getName());
            try
            {
               Thread.sleep(1000);
            } catch (InterruptedException e)
            {
               e.printStackTrace();
            }
            System.out.println("Lets add another time : " + ((MyThread) r).getName());
            x.execute(r);
    	});
    	
    	ctpe.prestartAllCoreThreads();
    	
    	while(true) {
    		threadCnt++;
    		ctpe.execute(new MyThread(String.valueOf(threadCnt)));
    		if(threadCnt==1000) break;
    	}
    }
    public static void threadPoolExample() {
    	final int poolSize=10,maxPoolSize=20;
    	int threadCnt = 0;
    	final long waitTime = 5000;
    	BlockingQueue<Runnable> bqueue = new LinkedBlockingQueue<Runnable>(50);
    	CustomThreadPoolExecutor ctpe = new CustomThreadPoolExecutor(poolSize, maxPoolSize, waitTime, TimeUnit.MILLISECONDS, bqueue);
    	
    	ctpe.setRejectedExecutionHandler((r,x)->{
    		System.out.println("MyThread Rejected : " + ((MyThread) r).getName());
            try
            {
               Thread.sleep(1000);
            } catch (InterruptedException e)
            {
               e.printStackTrace();
            }
            System.out.println("Lets add another time : " + ((MyThread) r).getName());
            x.execute(r);
    	});
    	
    	ctpe.prestartAllCoreThreads();
    	
    	while(true) {
    		threadCnt++;
    		ctpe.execute(new MyThread(String.valueOf(threadCnt)));
    		if(threadCnt==1000) break;
    	}
    }
    private static void checkTasks() throws Exception {
        if (taskOneResults == null
                || taskOneResults.isDone()
                || taskOneResults.isCancelled())
        {
            taskOneResults = executor.submit(new MyThread("myThread"));
        }
        if(taskOneResults!=null) System.out.print(taskOneResults.get());
    }
    public static void execServTest() {
    	executor = Executors.newFixedThreadPool(2);
    	while (true)
        {
            try
            {
                checkTasks();
                Thread.sleep(1000);
            } catch (Exception e) {
                System.err.println("Caught exception: " + e.getMessage());
            }
        }
    }
    public static void factCallableTest() {
    	int numberOfThreads = Runtime.getRuntime().availableProcessors();
    	System.out.println("Total Threads :"+numberOfThreads);
    	ThreadPoolExecutor tpe = (ThreadPoolExecutor) Executors.newFixedThreadPool(numberOfThreads);
    	List<Future<Integer>> resultList = new ArrayList<Future<Integer>>();
    	Random random = new Random();
    	for(int i=0;i<4;i++) {
    		Integer number = random.nextInt(10);
    		Future<Integer> result  = tpe.submit(new FactorialCalculator(number));
    		resultList.add(result);    		
    	}
    	resultList.forEach(x->{
			try {
				System.out.println(x.get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		});
    	tpe.shutdown();
    }
	public static void hashMapTest() {
		HashMap<Integer,String> m = new HashMap<Integer,String>();
		m.put(1, "A");
	}
	public static String mySubstring(int index, String original) {
		return new String(original.substring(index));	
		}
	public static void memoryLeakTest() {
		String s = "i_love_java";
		String t = s.substring(7);
		System.out.println(s);
		System.out.println(t);
		
		try {
			Field innerCharArr  = String.class.getDeclaredField("value");
			/*cannot access need to check*/
			innerCharArr.setAccessible(true);
			char[] seq = (char[]) innerCharArr.get(s);
			System.out.println(Arrays.toString(seq));
			
			seq = (char[]) innerCharArr.get(t);
			System.out.println(Arrays.toString(seq));
			/*issue has been fixed in 1.7 onwards*/
			
			System.out.println(mySubstring(7,s));
			
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
	}
	private static void someMethod() {
		System.out.println("--in someMethod --");
		//System.exit(1);/*terminates program*/
	}
	
	public static void finallyBlockTest() {
		System.out.println("--in finallyBlockTest start --");
		try {
			System.out.println("--in try --");
			return;/*completes method call from here & executes finally block*/
			//someMethod();
		} catch (Exception e) {
			System.out.println("--in catch --");
		}
		finally {
			System.out.println("--in finally --");
		}
		System.out.println("--in finallyBlockTest end --");
	}
	public static void main(String[] args) {
		blockingThreadPoolExample();
	}
	public static void jdbcExample() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;
//		String query = "select eed_id AS ID,eed_exm_id AS EXM_ID,eed_mdm_id AS Module,eed_tst_dt AS TEST_DATE from exmn_enrllmnt_dtls where eed_id=?  order by 1 limit 10";
		String query = "update exmn_enrllmnt_dtls set eed_pin=1234 where eed_id=?";
		try {
			Class.forName("org.postgresql.Driver");
			// jdbc:postgresql://host:port/database
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/NARMADA", "thrdexams", "thrdexams");
			if (isDQL(query)) {
				pstmt = conn.prepareStatement(query);
				pstmt.setLong(1, 2969);
				rs = pstmt.executeQuery();
			} else {
				pstmt = conn.prepareStatement(query);
				pstmt.setLong(1, 2969);
				pstmt.executeUpdate();
			}
			rsmd = pstmt.getMetaData();
			if (rsmd != null)
				for (int i = 1; i <=rsmd.getColumnCount(); i++)
					System.out.print(rsmd.getColumnLabel(i).toUpperCase() + "(" + rsmd.getColumnTypeName(i) + ")\t");
			System.out.println();
			while (rs!=null && rs.next())
				System.out.println(rs.getLong(1) + "\t\t" + rs.getString(2));
		
			if(rs!=null) rs.close();
			if(pstmt!=null) pstmt.close();
			if(conn!=null) conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static String columnSQLTypeName(int type) { return switch (type) {
	 * case Types.NUMERIC -> "NUMERIC"; case Types.VARCHAR -> "VARCHAR"; case
	 * Types.BINARY -> "BINARY"; default -> null; }; }
	 */	
	public static boolean isDQL(String query) {
		int dqlIndex = (query.contains("select") || query.contains("with"))? query.toUpperCase().indexOf("SELECT"):-1 ;
		return dqlIndex>-1;
	}

}
