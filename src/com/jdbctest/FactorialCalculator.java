package com.jdbctest;

import java.util.concurrent.Callable;

public class FactorialCalculator implements Callable<Integer> {
	private Integer number;
	public FactorialCalculator(Integer number) {
		this.number = number;
	} 
	@Override
	public Integer call() throws Exception {
		return factorial(number);
	}

	private Integer factorial(Integer number)
	{
		return (number == 1 || number == 0) ? 1 : number * factorial(number - 1);
	}
}
