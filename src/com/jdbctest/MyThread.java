package com.jdbctest;

public class MyThread implements Runnable {
	private String name;
	public MyThread(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}

	@Override
	public void run() {
//		while (true)
//        {
            System.out.println("Executing "+name);
            try
            {
                Thread.sleep(1000);
            } catch (Throwable e) {
                e.printStackTrace();
            }
//        }
	}

}
